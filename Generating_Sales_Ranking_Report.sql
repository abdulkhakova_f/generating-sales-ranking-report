WITH RankedCustomers AS (
  SELECT
  channel_id,
  calendar_month_desc,
    SUM (amount_sold) AS amount_sold,
    RANK() OVER (PARTITION BY channel_id, order_year ORDER BY SUM(amount_sold) DESC) AS sales_rank
  FROM
    sh.sales s
  WHERE
    calendar_month_desc IN ('1998', '1999', '2001')
  GROUP BY
    cust.city_id,
  channel_desc,
  calendar_month_desc
)

SELECT
  channel-id,
  calendar_month_desc,
  cust.city_id,
  amount_sold
FROM
  RankedCustomers
WHERE
  sales_rank <= 300;